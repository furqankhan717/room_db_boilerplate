package com.codementor.furqan.roomdatabase

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.codementor.furqan.roomdatabase.entities.User

/**
 * Activity for entering a word.
 */

class NewUserActivity : AppCompatActivity() {

    private lateinit var firstName: EditText
    private lateinit var lastName: EditText
    private lateinit var age: EditText

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_word)
        firstName = findViewById(R.id.firstName)
        lastName = findViewById(R.id.lastName)
        age = findViewById(R.id.age)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(firstName.text)) {

                showError("First Name cannot empty", firstName)
            } else if (TextUtils.isEmpty(lastName.text)) {

                showError("Last Name cannot empty", lastName)
            } else if (TextUtils.isEmpty(age.text)) {

                showError("Age  cannot empty", age)
            } else {
                val user: User = User()
                user.firstName = firstName.text.toString()
                user.lastName = lastName.text.toString()
                user.age = age.text.toString().toInt()
                replyIntent.putExtra(EXTRA_REPLY, user)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    fun showError(err: String, view: EditText) {
        view.error = err
        view.requestFocus()
    }

    companion object {
        const val EXTRA_REPLY = "com.example.android.wordlistsql.REPLY"
    }
}

